<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('books', function (Blueprint $table) {
            $table->increments('book_id');
            $table->string('name');
            $table->text('image');
            $table->text('thumbnail');
            $table->text('description')->nullable();
            $table->integer('publisher')->nullable();
            $table->integer('author')->nullable();
            $table->integer('category')->nullable();
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->string('file');
            $table->tinyInteger('status')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');

        Schema::table('books', function($table) {
            $table->dropColumn('category');
        });
    }
}
