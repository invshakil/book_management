<?php

use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            'name' => 'portfolio',
            'description' => 'portfolio portfolio portfolio',
            'publisher' => 1,
            'author' => 1,
            'file' => 'public/uploads/files/Web_Dev_Shakil.pdf',
            'status' => 1
        ]);
    }
}
