<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * WEB VIEW ROUTES
 */

Route::get('/', ['as' => 'home', 'uses' => 'webController@index']);
Route::get('/book-details/{id}/{name}', ['as' => 'book.details', 'uses' => 'webController@bookDetails']);


Route::get('/login', ['as' => 'user.login', 'uses' => 'userLogin@index']);

Route::post('/loginCheck', ['as' => 'user.login.check', 'uses' => 'userLogin@ajax_login']);

Route::get('/logout', ['as' => 'logout', 'uses' => 'userLogin@logout']);

Route::get('/redirect', 'SocialLoginController@redirect');

Route::get('/callback', 'SocialLoginController@callback');

/*
 * Admin Routes
 */

Route::get('/admin_dashboard', ['as' => 'admin_dashboard', 'uses' => 'admin@index']);
Route::get('/admin_dashboard/profile', ['as' => 'admin.profile', 'uses' => 'admin@profile']);
Route::post('/admin_dashboard/profile/{image}', ['as' => 'profile.image', 'uses' => 'admin@profile']);
Route::post('/admin_dashboard/profile/{do_update}', ['as' => 'profile.update', 'uses' => 'admin@profile']);

/*
 * Book Category CRUD
 */

Route::get('/admin_dashboard/book_category', ['as' => 'book.category', 'uses' => 'admin@bookCategory']);
Route::post('/admin_dashboard/book_category/{create}', ['as' => 'category.create', 'uses' => 'admin@bookCategory']);
Route::post('/admin_dashboard/book_category/{do_update}/{id}', ['as' => 'category.update', 'uses' => 'admin@bookCategory']);
Route::get('/admin_dashboard/book_category/{delete}/{id}', ['as' => 'category.delete', 'uses' => 'admin@bookCategory']);

/*
 * Book CRUD
 */

Route::get('/admin_dashboard/books', ['as' => 'books', 'uses' => 'admin@books']);
Route::post('/admin_dashboard/books/{create}', ['as' => 'books.create', 'uses' => 'admin@books']);
Route::post('/admin_dashboard/books/{do_update}/{id}', ['as' => 'books.update', 'uses' => 'admin@books']);
Route::get('/admin_dashboard/books/{delete}/{id}', ['as' => 'books.delete', 'uses' => 'admin@books']);

/*
 * PUBLISHERS CRUD
 */


Route::get('/admin_dashboard/publishers', ['as' => 'publishers', 'uses' => 'admin@publishers']);
Route::post('/admin_dashboard/publishers/{create}', ['as' => 'publishers.create', 'uses' => 'admin@publishers']);
Route::post('/admin_dashboard/publishers/{do_update}/{id}', ['as' => 'publishers.update', 'uses' => 'admin@publishers']);
Route::get('/admin_dashboard/publishers/{delete}/{id}', ['as' => 'publishers.delete', 'uses' => 'admin@publishers']);

/*
 * AUTHORS CRUD
 */


Route::get('/admin_dashboard/authors', ['as' => 'authors', 'uses' => 'admin@authors']);
Route::post('/admin_dashboard/authors/{create}', ['as' => 'authors.create', 'uses' => 'admin@authors']);
Route::post('/admin_dashboard/authors/{do_update}/{id}', ['as' => 'authors.update', 'uses' => 'admin@authors']);
Route::get('/admin_dashboard/authors/{delete}/{id}', ['as' => 'authors.delete', 'uses' => 'admin@authors']);

/*
 * !Admin Routes
 */
Route::get('/author_dashboard', ['as' => 'author_dashboard', 'uses' => 'author@index']);
Route::get('/publisher_dashboard', ['as' => 'publisher_dashboard', 'uses' => 'publisher@index']);
Route::get('/user_dashboard', ['as' => 'publisher_dashboard', 'uses' => 'user@index']);
