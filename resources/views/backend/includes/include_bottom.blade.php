<!-- Imported styles on this page -->
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="{{URL::to('/public/assets')}}/js/gsap/main-gsap.js"></script>
<script src="{{URL::to('/public/assets')}}/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/bootstrap.js"></script>
<script src="{{URL::to('/public/assets')}}/js/joinable.js"></script>
<script src="{{URL::to('/public/assets')}}/js/resizeable.js"></script>
<script src="{{URL::to('/public/assets')}}/js/neon-api.js"></script>
<script src="{{URL::to('/public/assets')}}/js/jquery.dataTables.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/TableTools.min.js"></script>


<!-- Imported scripts on this page -->
<script src="{{URL::to('/public/assets')}}/js/dataTables.bootstrap.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/lodash.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="{{URL::to('/public/assets')}}/js/select2/select2.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/neon-chat.js"></script>
<script src="{{URL::to('/public/assets')}}/js/fileinput.js"></script>

<script src="{{URL::to('/public/assets')}}/js/ckeditor/ckeditor.js"></script>
{{--<script src="{{URL::to('/public/assets')}}/js/ckeditor/adapters/jquery.js"></script>--}}

<!-- JavaScripts initializations and stuff -->
<script src="{{URL::to('/public/assets')}}/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="{{URL::to('/public/assets')}}/js/neon-demo.js"></script>
