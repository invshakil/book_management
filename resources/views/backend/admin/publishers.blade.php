@extends('backend.master')

@section('title')
    Book Publishers
@endsection

@section('main_content')
    <div class="panel minimal minimal-gray">

        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="form-group">
            @if(Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }} {{ Session::put('message',null) }}</div>
            @endif
        </div>
        <div class="form-group">
            @if(Session::has('exception'))
                <div class="alert alert-danger">{{ Session::get('exception') }} {{ Session::put('exception',null) }}</div>
            @endif
        </div>

        <div class="panel-heading">
            <div class="panel-title"><h2>Publishers Add/Manage</h2></div>
            <div class="panel-options">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile-1" data-toggle="tab">Publishers List</a></li>
                    <li><a href="#profile-2" data-toggle="tab">Add Publisher</a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <div class="tab-content">
                <div class="tab-pane active" id="profile-1">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Publishers List</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body table-responsive">

                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th width="20%">logo</th>
                                    <th width="20%">Name</th>
                                    <th width="20%">address</th>
                                    <th width="10%">Phone</th>
                                    <th width="10%">Status</th>
                                    <th width="10%">Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $result = DB::table('publishers')->get();?>

                                @foreach($result->all() as $row)
                                    <tr>

                                        <td class="center">{{ $row->publishers_id }}</td>
                                        <td class="center"><img src="{{ URL::to($row->logo) }}" width="30"></td>
                                        <td class="center">{{ $row->publishers_name }}</td>
                                        <td class="center">{{ $row->address }}</td>
                                        <td class="center">{{ $row->phone }}</td>
                                        <td class="center">
                                            @if($row->status==1)
                                                <div class="label label-success">Published</div>
                                            @else
                                                <div class="label label-danger">Pending</div>
                                            @endif
                                        </td>
                                        <td class="center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#" data-toggle="modal"
                                                           data-target="#myModal{{$row->publishers_id}}">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#" data-toggle="modal"
                                                           data-target="#confirm-delete{{$row->publishers_id}}">
                                                            <i class="fa fa-trash-o"></i>
                                                            delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>


                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="profile-2">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Add Publishers</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body">

                            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                  action="{{ URL::to('/admin_dashboard/publishers/create') }}"
                                  enctype="multipart/form-data">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Publishers Name</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="publishers_name" class="form-control" id="field-1"
                                               placeholder="Enter Publishers Name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-ta" class="col-sm-3 control-label">Address</label>

                                    <div class="col-sm-5">
                                        <textarea class="form-control" name="address" id="field-ta"
                                                  placeholder="Enter Address"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Logo</label>

                                    <div class="col-sm-5">

                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; "
                                                 data-trigger="fileinput">
                                                <img src="http://placehold.it/800x600" alt="...">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                 style="max-width: 200px; max-height: 150px"></div>
                                            <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                                <a href="#" class="btn btn-orange fileinput-exists"
                                                   data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>

                                    </div>
                                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Image size maximum 300kb.</span>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Publishers Phone</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="phone" class="form-control" id="field-1"
                                               placeholder="Enter Phone Number">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Publishers Email</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="email" class="form-control" id="field-1"
                                               placeholder="Enter Publishers Email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Publishers Website</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="website" class="form-control" id="field-1"
                                               placeholder="Enter Publishers Website">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Publication Status</label>

                                    <div class="col-sm-5">
                                        <select name="status" class="form-control">
                                            <option value="1">Published</option>
                                            <option value="0">Pending</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary">Add Publishers</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    <script type="text/javascript">
        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var tableContainer;

        jQuery(document).ready(function ($) {
            tableContainer = $("#table-1");

            tableContainer.dataTable({
                "sPaginationType": "bootstrap",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true,


                // Responsive Settings
                bAutoWidth: false,
                fnPreDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                fnDrawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });

    </script>

    @include('backend.admin.modal.modal_edit_publishers')


@endsection