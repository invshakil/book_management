@extends('backend.master')

@section('title')
    Admin Profile Settings
@endsection

@section('main_content')


    <div class="panel minimal minimal-gray">
        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            @if(Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }} {{ Session::put('message',null) }}</div>
            @endif
        </div>

        <div class="panel panel-dark" data-collapsed="0">

            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">Profile Picture</div>

                <div class="panel-options">
                    <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                class="entypo-cog"></i></a>
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                    <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                </div>
            </div>

            <!-- panel body -->
            <div class="panel-body">
                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="{{ URL::to('/admin_dashboard/profile/image') }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Image Upload</label>

                        <div class="col-sm-5">

                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php
                                $id = Session::get('id');
                                $image = DB::table('users')->where('id', $id)->value('image_path');
                                ?>
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"
                                     data-trigger="fileinput">
                                    <?php if($image != ''){?>
                                    <img src="<?php echo URL::to($image); ?>" alt="...">
                                    <?php } else{?>
                                    <img src="http://placehold.it/200x150" alt="...">
                                    <?php }?>
                                </div>

                                <div class="fileinput-preview fileinput-exists thumbnail"
                                     style="max-width: 200px; max-height: 150px">

                                </div>

                                <div>
											<span class="btn btn-white btn-file">

                                                <?php if ($image != ''){?>
                                                <span class="fileinput-new">Update Image</span>
                                                <?php }else{?>
                                                <span class="fileinput-new">Select Image</span>
                                                <?php }?>

                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                    <a href="#" class="btn btn-orange fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>

                                <input type="hidden" name="id" value="{{ Session::get('id') }}">
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Save Image</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel panel-dark" data-collapsed="0">

            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">Profile Information</div>

                <div class="panel-options">
                    <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                class="entypo-cog"></i></a>
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                    <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                </div>
            </div>

            <!-- panel body -->
            <div class="panel-body">

                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="{{ URL::to('admin_dashboard/profile/do_update') }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <?php
                    $id = Session::get('id');
                    $info = DB::table('users')->where('id', $id)->first();
                    ?>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Profession</label>

                        <div class="col-sm-5">
                            <input type="text" name="profession" class="form-control"
                                   value="<?php echo $info->profession;?>" id="field-1"
                                   placeholder="Your Profession">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Home Address</label>

                        <div class="col-sm-5">
                            <textarea class="form-control" name="address" id="field-ta"
                                      placeholder="Your Home Address"><?php echo $info->address;?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Phone</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="phone"
                                   value="<?php echo $info->phone;?>" id="field-1" placeholder="Your Contact Number">
                        </div>
                    </div>

                    <input type="hidden" name="id" value="{{ Session::get('id') }}">

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Save Profile Info</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

        <div class="panel panel-dark" data-collapsed="0">

            <!-- panel head -->
            <div class="panel-heading">
                <div class="panel-title">Change Password</div>

                <div class="panel-options">
                    <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                                class="entypo-cog"></i></a>
                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                    <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                </div>
            </div>

            <!-- panel body -->
            <div class="panel-body">

                <form role="form" class="form-horizontal form-groups-bordered" method="post"
                      action="{{ URL::to('admin_dashboard/profile/password') }}" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">New Password</label>

                        <div class="col-sm-5">
                            <input type="password" name="password" class="form-control"  id="field-1"
                                   placeholder="Enter New Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Type Password Again</label>

                        <div class="col-sm-5">
                            <input type="password" class="form-control" name="confirm_password" id="field-1" placeholder="Type Password Again">
                        </div>
                    </div>

                    <input type="hidden" name="id" value="{{ Session::get('id') }}">

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-success">Change Current Password</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>

    <script type="text/javascript">
        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var tableContainer;

        jQuery(document).ready(function ($) {
            tableContainer = $("#table-1");

            tableContainer.dataTable({
                "sPaginationType": "bootstrap",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true,


                // Responsive Settings
                bAutoWidth: false,
                fnPreDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                fnDrawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });
    </script>


@endsection