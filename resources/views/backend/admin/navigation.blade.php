<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{URL::to('/public/assets')}}/images/ebook1.png" width="120" alt=""/>
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon">
                    <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="@if(Request::url() == route('admin_dashboard')) active @endif" >
                <a href="{{ URL::to('admin_dashboard') }}" >
                    <i class="fa fa-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- PROFILE -->
            <li class="@if(Request::url() == route('admin.profile')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/profile') }}" >
                    <i class="fa fa-id-card"></i>
                    <span>Profile</span>
                </a>
            </li>

            <!-- BOOKS -->
            <li class="@if(Request::url() == route('book.category') || Request::url() == route('books'))) opened active @endif">
                <a href="#">
                    <i class="fa fa-book"></i>
                    <span>Book Management</span>
                </a>
                <ul>
                    <li class="@if(Request::url() == route('book.category')) active @endif" >
                        <a href="{{ URL::to('admin_dashboard/book_category') }}" >
                            <i class="fa fa-id-card"></i>
                            <span>Book Category</span>
                        </a>
                    </li>
                    <li class="@if(Request::url() == route('books')) active @endif" >
                        <a href="{{ URL::to('admin_dashboard/books') }}" >
                            <i class="fa fa-id-card"></i>
                            <span>Books</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Publishers -->
            <li class="@if( Request::url() == route('publishers')) opened active has-sub @endif">
                <a href="#">
                    <i class="fa fa-bookmark-o"></i>
                    <span>Publishers Management</span>
                </a>
                <ul>

                    <li class="@if(Request::url() == route('publishers')) active @endif" >
                        <a href="{{ URL::to('admin_dashboard/publishers') }}" >
                            <i class="fa fa-bookmark-o"></i>
                            <span>Publishers</span>
                        </a>
                    </li>
                </ul>
            </li>

            <!-- Authors -->
            <li class="@if(Request::url() == route('authors')) opened active has-sub @endif">
                <a href="#">
                    <i class="fa fa-id-card"></i>
                    <span>Authors Management</span>
                </a>
                <ul>
                    <li class="@if(Request::url() == route('authors')) active @endif" >
                        <a href="{{ URL::to('admin_dashboard/authors') }}" >
                            <i class="fa fa-id-card"></i>
                            <span>Authors</span>
                        </a>
                    </li>
                </ul>
            </li>




        </ul>

    </div>

</div>