<?php $result = DB::table('publishers')->get(); ?>

@foreach($result->all() as $row)
    <!-- Modal -->
    <div class="modal fade" id="myModal{{ $row->publishers_id }}" tabindex="-1" role="dialog"
         aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit
                        Publishers: {{ $row->publishers_name }}
                        Information</h4>
                </div>
                <div class="modal-body">

                    <form role="form" class="form-horizontal form-groups-bordered" method="post"
                          action="{{ URL::to('/admin_dashboard/publishers/do_update'.'/'.$row->publishers_id) }}"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Publishers Name</label>

                            <div class="col-sm-5">
                                <input type="text" name="publishers_name" value="{{ $row->publishers_name }}"
                                       class="form-control" id="field-1"
                                       placeholder="Enter Publishers Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-ta" class="col-sm-3 control-label">Address</label>

                            <div class="col-sm-5">
                                        <textarea class="form-control" name="address" id="field-ta"
                                                  placeholder="Enter Address">{{ $row->address }}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Logo</label>

                            <div class="col-sm-5">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; "
                                         data-trigger="fileinput">
                                        @if($row->logo)
                                            <img src="{{ URL::to($row->logo) }}">
                                        @else
                                            <img src="http://placehold.it/200x150">
                                        @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 200px; max-height: 150px"></div>
                                    <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                        <a href="#" class="btn btn-orange fileinput-exists"
                                           data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>

                            </div>
                            <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Image size maximum 300kb.</span>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Publishers Phone</label>

                            <div class="col-sm-5">
                                <input type="text" name="phone" value="{{ $row->phone }}" class="form-control"
                                       id="field-1"
                                       placeholder="Enter Phone Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Publishers Email</label>

                            <div class="col-sm-5">
                                <input type="text" name="email" value="{{ $row->email }}" class="form-control"
                                       id="field-1"
                                       placeholder="Enter Publishers Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Publishers Website</label>

                            <div class="col-sm-5">
                                <input type="text" name="website" value="{{ $row->website }}" class="form-control"
                                       id="field-1"
                                       placeholder="Enter Publishers Website">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publication Status</label>

                            <div class="col-sm-5">
                                <select name="status" class="form-control">
                                    <option value="1" @if($row->status == 1) selected @endif>Published</option>
                                    <option value="0" @if($row->status == 0) selected @endif>Pending</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-primary">Update Publishers</button>
                            </div>
                        </div>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!-- (Normal Modal)-->
    <div class="modal fade" id="confirm-delete{{$row->publishers_id}}">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="{{ URL::to('/admin_dashboard/publishers/delete/').'/'.$row->publishers_id }}"
                       class="btn btn-danger btn-ok">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

@endforeach