<?php $result = DB::table('books')
        ->join('publishers', 'books.publisher', '=', 'publishers.publishers_id')
        ->join('authors', 'books.author', '=', 'authors.authors_id')
        ->select('books.*', 'publishers.publishers_name', 'authors.authors_name')
        ->get(); ?> ?>

@foreach($result->all() as $row)
    <!-- Modal -->
    <div class="modal fade" id="myModal{{ $row->book_id}}" tabindex="-1" role="dialog"
         aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit
                        Author's: {{ $row->name }}
                        Information</h4>
                </div>
                <div class="modal-body">

                    <form role="form" class="form-horizontal form-groups-bordered" method="post"
                          action="{{ URL::to('/admin_dashboard/books/do_update'.'/'.$row->book_id) }}"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Book's Name</label>

                            <div class="col-sm-5">
                                <input type="text" name="name" class="form-control" id="field-1"
                                       value="{{ $row->name }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Book Cover Image</label>

                            <div class="col-sm-5">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; "
                                         data-trigger="fileinput">
                                        @if($row->thumbnail)
                                            <img src="{{ URL::to($row->thumbnail) }}">
                                        @else
                                            <img src="http://placehold.it/200x150">
                                        @endif
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 200px; max-height: 150px"></div>
                                    <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                        <a href="#" class="btn btn-orange fileinput-exists"
                                           data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>

                            </div>
                            <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Image size maximum 300kb.</span>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Description about this
                                book</label>

                            <div class="col-sm-8">
                                <textarea name="description" class="form-control ckeditor">{{$row->description}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Select Category</label>

                            <div class="col-sm-5">
                                <select name="category" class="form-control select2">
                                    {{ $cat = DB::table('categories')->where('status',1)->get() }}
                                    @foreach($cat as $row1)
                                        <option value="{{ $row1->category_id }}"
                                                @if($row->category == $row1->category_id) selected @endif
                                        >{{ $row1->category_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Select Publisher</label>

                            <div class="col-sm-5">
                                <select name="publisher" class="form-control select2">
                                    {{ $pub = DB::table('publishers')->where('status',1)->select('publishers_id','publishers_name')->get() }}
                                    @foreach($pub as $row1)
                                        <option value="{{ $row1->publishers_id }}"
                                                @if($row->publisher == $row1->publishers_id) selected @endif
                                        >{{ $row1->publishers_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Select Author</label>

                            <div class="col-sm-5">
                                <select name="author" class="form-control select2">
                                    {{ $aut = DB::table('authors')->where('status',1)->select('authors_id','authors_name')->get() }}
                                    @foreach($aut as $row1)
                                        <option value="{{ $row1->authors_id }}"
                                                @if($row->author == $row1->authors_id) selected @endif
                                        >{{ $row1->authors_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Meta Title</label>

                            <div class="col-sm-5">
                                        <textarea class="form-control" name="meta_title" id="field-ta"
                                                  placeholder="Your Meta Title">{{$row->meta_title}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Meta Description</label>

                            <div class="col-sm-5">
                                        <textarea class="form-control" name="meta_description" id="field-ta"
                                                  placeholder="Your Meta Description">{{$row->meta_description}}</textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publication Status</label>

                            <div class="col-sm-5">
                                <select name="status" class="form-control">
                                    <option value="1" @if($row->status == 1) selected @endif>Published</option>
                                    <option value="0" @if($row->status == 0) selected @endif>Pending</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-primary">Update Book Information</button>
                            </div>
                        </div>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!-- (Normal Modal)-->
    <div class="modal fade" id="confirm-delete{{$row->book_id}}">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="{{ URL::to('/admin_dashboard/books/delete/').'/'.$row->book_id }}"
                       class="btn btn-danger btn-ok">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

@endforeach