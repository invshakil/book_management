<?php $result = DB::table('categories')->get(); ?>

@foreach($result->all() as $row)
    <!-- Modal -->
    <div class="modal fade" id="myModal{{ $row->category_id }}" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit
                        Category: {{ $row->category_name }}
                        Information</h4>
                </div>
                <div class="modal-body">

                    <form role="form"
                          class="form-horizontal form-groups-bordered"
                          method="post"
                          action="{{ URL::to('/admin_dashboard/book_category/do_update/'.$row->category_id) }}"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="field-1">Category
                                Name</label>

                            <div class="">
                                <input type="text" name="category_name"
                                       value="{{ $row->category_name }}"
                                       class="form-control" id="field-1"
                                       placeholder="Enter Category Name">
                            </div>
                        </div>
                        <br/>

                        <div class="form-group">
                            <label for="field-ta">Category
                                Description</label>

                            <div class="">
                            <textarea class="form-control" cols="20" rows="5" name="category_description"
                                      id="field-ta"
                                      placeholder="Enter description">{{ $row->category_description }}</textarea>
                            </div>
                        </div>
                        <br/>

                        <div class="form-group">
                            <label>Publication
                                Status</label>

                            <div class="">
                                <select name="status" class="form-control">
                                    <option value="1"
                                            @if($row->status == 1) selected @endif>
                                        Published
                                    </option>
                                    <option value="0"
                                            @if($row->status == 0) selected @endif>
                                        Pending
                                    </option>
                                </select>
                            </div>
                        </div>
                        <br/>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">
                                    Update Category
                                </button>
                            </div>
                        </div>
                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!-- (Normal Modal)-->
    <div class="modal fade" id="confirm-delete{{$row->category_id}}">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="{{ URL::to('/admin_dashboard/book_category/delete/').'/'.$row->category_id }}"
                       class="btn btn-danger btn-ok">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

@endforeach