<!DOCTYPE html>
<html lang="en">

@include('frontend.includes.header_script')

<body class="home01">
<!--Header-->
<div class="header">
    <!--Header-Top-->
@include('frontend.includes.top_banner')
<!--/.header-top-->
    <!--Header-Middle-->
@include('frontend.includes.large_banner')
<!--/.header-middle-->
    <!--Navbar-->
@include('frontend.includes.navigation')
<!--/.navbar-->
</div>
<!--/.header-->
<!--Slider-area-->

<!--/.slider-area-->
<!--Site-Content-->
<div id="#content" class="site-content">
    <!-- container-->

    <div class="container">
        @yield('main_content')
    </div>
    <!--/.container-->
</div>
<!--/.site-content-->
<!--Footer-->
@include('frontend.includes.footer')
<!--/.footer-->
<!--==============================
        Footer js pluging -->
<!-- jQuery -->
<script src="{{URL::to ('public/web_assets')}}/assets/js/jquery-1.12.0.min.js"></script>
<!-- modernizr -->
<script src="{{URL::to ('public/web_assets')}}/assets/js/vendor/modernizr-2.8.3.min.js"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="{{URL::to ('public/web_assets')}}/assets/js/bootstrap.min.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript"
        src="{{URL::to ('public/web_assets')}}/assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript"
        src="{{URL::to ('public/web_assets')}}/assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- wow js-->
<script type="text/javascript" src="{{URL::to ('public/web_assets')}}/assets/js/wow.min.js"></script>
<!-- venobox js-->
<script type="text/javascript" src="{{URL::to ('public/web_assets')}}/assets/js/venobox.min.js"></script>
<!-- mouse hover js-->
<script src="{{URL::to ('public/web_assets')}}/assets/js/jquery.directional-hover.js"></script>
<!-- owl js -->
<script src="{{URL::to ('public/web_assets')}}/assets/js/owl.carousel.min.js"></script>
<!-- magnific popup -->
<script src="{{URL::to ('public/web_assets')}}/assets/js/jquery.magnific-popup.min.js"></script>
<!-- smoothscroll -->
<script src="{{URL::to ('public/web_assets')}}/assets/js/smoothscroll.js"></script>
<!-- settings -->
<script type="text/javascript" src="{{URL::to ('public/web_assets')}}/assets/js/setting.js"></script>
<script type="text/javascript" src="{{URL::to ('public/web_assets')}}/assets/js/setting-revolution-1.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/css/star-rating.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.2/js/star-rating.min.js"></script>
</body>
</html>