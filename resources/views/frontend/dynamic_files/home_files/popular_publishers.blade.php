<div class="col-md-4 col-sm-6 col-xs-6 top-sellers">
    <div class="items-carosel-single">
        <div class="heading-title">
            <h3 class="title-text">POPULAR PUBLISHERS</h3>
        </div>
        <!--/.heading-title-->
        <div id="topsell-items-slide">
            <ul class="item-list">
                <li class="item">
                    <div class="product-thumb"><img
                                src="{{URL::to ('public/web_assets')}}/assets/images/small_gallery/10.jpg"
                                alt="img">
                    </div>
                    <div class="product-info">
                        <h2>White Stripe T-Shirt</h2>
                        <div class="price"> $40
                            <del> $50</del>
                        </div>
                        <a class="cart-btn btn" href="single-product.html">add to cart</a>
                    </div>
                </li>
                <!--/.item-->
                <li class="item">
                    <div class="product-thumb"><img
                                src="{{URL::to ('public/web_assets')}}/assets/images/small_gallery/11.jpg"
                                alt="img">
                    </div>
                    <div class="product-info">
                        <h2>White Stripe T-Shirt</h2>
                        <div class="price"> $40
                            <del> $50</del>
                        </div>
                        <a class="cart-btn btn" href="single-product.html">add to cart</a>
                    </div>
                </li>
                <!--/.item-->
                <li class="item">
                    <div class="product-thumb"><img
                                src="{{URL::to ('public/web_assets')}}/assets/images/small_gallery/03.jpg"
                                alt="img">
                    </div>
                    <div class="product-info">
                        <h2>White Stripe T-Shirt</h2>
                        <div class="price"> $40
                            <del> $50</del>
                        </div>
                        <a class="cart-btn btn" href="single-product.html">add to cart</a>
                    </div>
                </li>
                <!--/.item-->
            </ul>
            <!--/.item-list-->
            <ul class="item-list">
                <li class="item">
                    <div class="product-thumb"><img
                                src="{{URL::to ('public/web_assets')}}/assets/images/small_gallery/04.jpg"
                                alt="img">
                    </div>
                    <div class="product-info">
                        <h2>White Stripe T-Shirt</h2>
                        <div class="price"> $40
                            <del> $50</del>
                        </div>
                        <a class="cart-btn btn" href="single-product.html">add to cart</a>
                    </div>
                </li>
                <!--/.item-->
                <li class="item">
                    <div class="product-thumb"><img
                                src="{{URL::to ('public/web_assets')}}/assets/images/small_gallery/05.jpg"
                                alt="img">
                    </div>
                    <div class="product-info">
                        <h2>White Stripe T-Shirt</h2>
                        <div class="price"> $40
                            <del> $50</del>
                        </div>
                        <a class="cart-btn btn" href="single-product.html">add to cart</a>
                    </div>
                </li>
                <!--/.item-->
                <li class="item">
                    <div class="product-thumb"><img
                                src="{{URL::to ('public/web_assets')}}/assets/images/small_gallery/06.jpg"
                                alt="img">
                    </div>
                    <div class="product-info">
                        <h2>White Stripe T-Shirt</h2>
                        <div class="price"> $40
                            <del> $50</del>
                        </div>
                        <a class="cart-btn btn" href="single-product.html">add to cart</a>
                    </div>
                </li>
                <!--/.item-->
            </ul>
            <!--/.item-list-->
        </div>
        <!--/.topsell-items-slide-->
    </div>
    <!--/.items-carosel-single-->
</div>