<div class="latest-items">
    <div class="tab-header">
        <div class="heading-title">
            <h3 class="title-text">LATEST EBOOK COLLECTION</h3>
        </div>
        <!--/.tab-item-->
    </div>
    <!--/.tab-header-->
    <div class="tab-content row">
        <div id="new" class="tab-pane fade in active">

            @foreach($latest_books->chunk(4) as $resultChunk)
                <div class="container">
                    <div class="row">
                        @foreach($resultChunk as $item)
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="product-single">
                                    <div class="product-thumb">
                                        <img class="img-responsive" alt="Single product"
                                             src="{{URL::to ($item->image)}}" width="220">
                                        <div class="actions">
                                            <ul>
                                                <li><a class="add-cart" href="{{url('/book-details/'.base64_encode($item->book_id).'/'.str_replace(' ','-',$item->name) )}}"><span><span
                                                                    class="fa fa-check-circle"></span></span> Check Details</a>
                                                </li>

                                                <li class="pull-right"><a class="zoom"
                                                                          href="{{URL::to ($item->image)}}"><span
                                                                class="arrow_expand"></span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!--/.product-thumb-->
                                    <div class="product-info">
                                        <h2>{{ $item->name }} ({{$item->category_name}})</h2>
                                        <div class="price">
                                            <div class="pull-left">
                                                <i class="fa fa-user" aria-hidden="true"></i> Author: {{ $item->authors_name }}
                                            </div>
                                        </div>
                                    </div>
                                    <!--/.product-info-->
                                </div>
                                <!--/.product-single-->
                            </div>
                        @endforeach

                    </div>
                </div>

        @endforeach

        <!--/.col-md-3-->
        </div>
        <!--/.new-->
    </div>
</div>