<div class="latest-blogs">
    <div class="row">
        <div class="col-sm-12">
            <div class="heading-title">
                <h3 class="title-text">LATEST NEWS FROM THE BLOG</h3>
            </div>
            <!--/.heading-title-->
        </div>
        <!--/.col-md-12-->
        <div class="col-md-4 col-sm-6 col-xs-6">
            <article class="post">
                <div class="post-thumb-content">
                    <figure class="post-thumb">
                        <a href="#"> <img class="img-responsive" alt="thumb"
                                          src="{{URL::to ('public/web_assets')}}/assets/images/blog/blog-thumb1.jpg">
                        </a>
                    </figure>
                    <!--/.post thumb--><span class="entry-date">15th apr. 2016</span>
                </div>
                <!--/.post-thumb-content-->
                <div class="post-details">
                    <h3 class="entry-title">
                        <a href="blog-single.html">New men´s styles in 2016</a>
                    </h3>
                    <!--/.entry title-->
                    <div class="entry-meta"> <span>
                                        <img alt="icon"
                                             src="{{URL::to ('public/web_assets')}}/assets/images/cat-icon.png">
                                        in
                                        <a href="#">clothes</a>,
                                        <a href="#">fashion</a>,
                                        <a href="#">men</a>
                                        </span> <span>
                                        <img alt="icon"
                                             src="{{URL::to ('public/web_assets')}}/assets/images/comment-icon.png">
                                        <a href="#">19</a>
                                        </span>
                    </div>
                    <!--/.meta-->
                    <div class="entry-content">
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Morbi
                            mollis pellen tesque aug Phase llus ac venenl....... </p>
                    </div>
                    <!--/.entry content-->
                </div>
                <!--/.post details-->
            </article>
            <!--/.post-->
        </div>
        <!--/.col-md-4-->
        <div class="col-md-4 col-sm-6 col-xs-6">
            <article class="post">
                <div class="post-thumb-content">
                    <figure class="post-thumb">
                        <a href="#"> <img class="img-responsive" alt="thumb"
                                          src="{{URL::to ('public/web_assets')}}/assets/images/blog/blog-thumb2.jpg">
                        </a>
                    </figure>
                    <!--/.post thumb--><span class="entry-date">15th apr. 2016</span>
                </div>
                <!--/.post-thumb-content-->
                <div class="post-details">
                    <h3 class="entry-title">
                        <a href="blog-single.html">New men´s styles in 2016</a>
                    </h3>
                    <!--/.entry title-->
                    <div class="entry-meta"> <span>
                                        <img alt="icon"
                                             src="{{URL::to ('public/web_assets')}}/assets/images/cat-icon.png">
                                        in
                                        <a href="#">clothes</a>,
                                        <a href="#">fashion</a>,
                                        <a href="#">men</a>
                                        </span> <span>
                                        <img alt="icon"
                                             src="{{URL::to ('public/web_assets')}}/assets/images/comment-icon.png">
                                        <a href="#">19</a>
                                        </span>
                    </div>
                    <!--/.meta-->
                    <div class="entry-content">
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Morbi
                            mollis pellen tesque aug Phase llus ac venenl....... </p>
                    </div>
                    <!--/.entry content-->
                </div>
                <!--/.post details-->
            </article>
            <!--/.post-->
        </div>
        <!--/.col-md-4-->
        <div class="col-md-4 col-sm-6 col-xs-6">
            <article class="post">
                <div class="post-thumb-content">
                    <figure class="post-thumb">
                        <a href="#"> <img class="img-responsive" alt="thumb"
                                          src="{{URL::to ('public/web_assets')}}/assets/images/blog/blog-thumb3.jpg">
                        </a>
                    </figure>
                    <!--/.post thumb--><span class="entry-date">15th apr. 2016</span>
                </div>
                <!--/.post-thumb-content-->
                <div class="post-details">
                    <h3 class="entry-title">
                        <a href="blog-single.html">New men´s styles in 2016</a>
                    </h3>
                    <!--/.entry title-->
                    <div class="entry-meta"> <span>
                                        <img alt="icon"
                                             src="{{URL::to ('public/web_assets')}}/assets/images/cat-icon.png">
                                        in
                                        <a href="#">clothes</a>,
                                        <a href="#">fashion</a>,
                                        <a href="#">men</a>
                                        </span> <span>
                                        <img alt="icon"
                                             src="{{URL::to ('public/web_assets')}}/assets/images/comment-icon.png">
                                        <a href="#">19</a>
                                        </span>
                    </div>
                    <!--/.meta-->
                    <div class="entry-content">
                        <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Morbi
                            mollis pellen tesque aug Phase llus ac venenl....... </p>
                    </div>
                    <!--/.entry content-->
                </div>
                <!--/.post details-->
            </article>
            <!--/.post-->
        </div>
        <!--/.col-md-4-->
    </div>
    <!--/.row-->
</div>