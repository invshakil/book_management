@extends('frontend.master')

@section('main_content')
    <div class="container">
        <div class="page_title_area row">
            <div class="col-md-12">
                <div class="bredcrumb">
                    <ul>
                        <li><a href="{{url('/')}}">HOME</a>
                        </li>
                        <li class="active"><a href="#">SHOP single page</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="single-produce-page no-sidebar">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="single-product-details clearfix">
                        <div class="col-md-4">
                            <div class="single-slider-item">
                                <img src="{{url($book_details->image)}}" alt="" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="right-content">
                                <div class="product-info">
                                    <h2>Book Name: {{ $book_details->name }}</h2>
                                    <h4 class="product-name"><i class="fa fa-bookmark" aria-hidden="true"></i>
                                        Category: {{ $book_details->category_name }}</h4>
                                    <div class="price">
                                        <h3><i class="fa fa-user" aria-hidden="true"></i>
                                            Author: {{$book_details->authors_name}}</h3>
                                    </div>
                                    <div class="price">
                                        <h3><i class="fa fa-users" aria-hidden="true"></i>
                                            Publishers: {{$book_details->publishers_name}}</h3>
                                    </div>

                                    <div class="actions">
                                        <ul>
                                            <li><a class="add-cart" href="single-product.html"><span><span
                                                                class="fa fa-check-circle"></span></span> Read This Book</a>
                                            </li>
                                            <li><a href="#"><span class="icon_heart_alt"></span> Save to your
                                                    account</a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="product-description">
                                        <h5 class="small-title">Rate THIS BOOK</h5>
                                        <form method="post" name="rating" action="{{url('/')}}">
                                            <input type="text" name="rating" class="rating rating-loading"
                                                   data-min="0" data-max="5" data-step="0.5" value=""
                                            ><br/>
                                            <input type="submit" class="btn btn-primary">
                                        </form>
                                    </div>
                                    <b>Hit Count: 100 hits</b>

                                    <br/><br/>
                                    <div class="actions">
                                        <ul>
                                            @php $tags= $book_details->tags; $items = explode(",",$tags);@endphp
                                            <b>Tags:</b>
                                            @foreach($items as $item)
                                                <li>
                                                    <a><span>{{$item}}</span></a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <br/>
                                        <div class="container">
                                            <h2>Share This Book</h2>
                                            <div class="social">
                                                <a href="#" id="share-fb" class="sharer button"><i
                                                            class="fa fa-3x fa-facebook-square"></i></a>
                                                <a href="#" id="share-tw" class="sharer button"><i
                                                            class="fa fa-3x fa-twitter-square"></i></a>
                                                <a href="#" id="share-li" class="sharer button"><i
                                                            class="fa fa-3x fa-linkedin-square"></i></a>
                                                <a href="#" id="share-gp" class="sharer button"><i
                                                            class="fa fa-3x fa-google-plus-square"></i></a>
                                                <a href="#" id="share-em" class="sharer button"><i
                                                            class="fa fa-3x fa-envelope-square"></i></a>
                                            </div>
                                        </div>

                                    </div>
                                    <br/>


                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="product-tab">
                                <ul class="nav nav-tabs">
                                    <li class="nav active"><a data-toggle="tab" href="#tab1">PRODUCT DESCRIPTION</a>
                                    </li>
                                    <li><a data-toggle="tab" href="#tab2">FACEBOOK COMMENT</a>
                                    </li>
                                    <li><a data-toggle="tab" href="#tab3">CLAIM COPYRIGHT</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div id="tab1" class="tab-pane active">
                                        <p align="justify">{!! $book_details->description !!}</p>
                                    </div>
                                    <div id="tab2" class="tab-pane">

                                        <div class="single-post-page">
                                            <div class="row">


                                                <!-- Comment-Area -->
                                                <div id="comments" class="comments-area">
                                                    <div class="comment-section">

                                                        <ol class="comment-list">
                                                            <li class="comment">
                                                                <div class="comment-body">
                                                                    <div class="comment-author vcard">
                                                                        <div class="author-img">
                                                                            <img alt=""
                                                                                 src="{{ url('public') }}/web_assets/assets/images/author/02.jpg">
                                                                        </div>
                                                                    </div>
                                                                    <!--/.comment-author-->
                                                                    <div class="comment-details">
                                                                        <div class="comment-metadata">
                                                                            <b class="author-name">Martin Jackson</b>
                                                                            <span class="date">April 14,2015 at 11:04</span>
                                                                        </div>
                                                                        <!--/.comment-metadata -->

                                                                        <div class="comment-content">
                                                                            <p>At vero eos et accusam et justo duo
                                                                                dolores
                                                                                et ea rebum. Stet clita kasd gubergren,
                                                                                no
                                                                                sea takimata sanctus est Lorem ipsum
                                                                                dolor
                                                                                sit amet. Lorem ipsum dolor sit amet,
                                                                                consetetur sadipscing elitr, sed diam
                                                                                nonumy
                                                                                eirmod tempor invidunt ut labore et
                                                                                dolore
                                                                                magna aliquyam erat, sed diam
                                                                                voluptua.</p>
                                                                        </div>
                                                                        <!--/.comment-content -->
                                                                        <div class="reply">
                                                                            <a class="comment-reply-link"
                                                                               href="#">Reply</a>
                                                                        </div>
                                                                        <!--/.reply -->
                                                                    </div>
                                                                    <!-- /.comment-details -->
                                                                </div>
                                                                <!-- .comment-body -->
                                                                <ol class="children">
                                                                    <li class="comment">
                                                                        <div class="comment-body">
                                                                            <div class="comment-author vcard">
                                                                                <div class="author-img">
                                                                                    <img alt="author"
                                                                                         src="{{ url('public') }}/web_assets/assets/images/author/01.jpg">
                                                                                </div>
                                                                            </div>
                                                                            <!--/.comment-author -->
                                                                            <div class="comment-details">
                                                                                <div class="comment-metadata">
                                                                                    <b class="author-name">Jonathan
                                                                                        Doe</b>
                                                                                    <span class="date">April 14,2015  at 11:04</span>
                                                                                    <a href="#"
                                                                                       class="user-name">author</a>
                                                                                </div>
                                                                                <!--/.comment-metadata -->
                                                                                <div class="comment-content">
                                                                                    <p>Duis autem vel eum iriure dolor
                                                                                        in
                                                                                        hendrerit in vulputate velit
                                                                                        esse
                                                                                        molestie consequat, vel illum
                                                                                        dolore
                                                                                        eu feugiat nulla facilisis.
                                                                                    </p>
                                                                                </div>
                                                                                <!--/.comment-content -->
                                                                                <div class="reply">
                                                                                    <a class="comment-reply-link"
                                                                                       href="#">Reply</a>
                                                                                </div>
                                                                                <!--/.reply -->
                                                                            </div>
                                                                            <!--/.comment-details -->

                                                                        </div>
                                                                        <!--/.comment-body -->
                                                                    </li>
                                                                </ol>
                                                            </li>

                                                            <li class="comment">
                                                                <div class="comment-body">
                                                                    <div class="comment-author vcard">
                                                                        <div class="author-img">
                                                                            <img alt=""
                                                                                 src="{{ url('public') }}/web_assets/assets/images/author/03.png">
                                                                        </div>
                                                                    </div>
                                                                    <!--/.comment-author -->
                                                                    <div class="comment-details">
                                                                        <div class="comment-metadata">
                                                                            <b class="author-name">Kevin Harting</b>
                                                                            <span class="date">April 14,2015 at 11:04</span>
                                                                        </div>
                                                                        <!--/.comment-metadata -->

                                                                        <div class="comment-content">
                                                                            <p>Lorem ipsum dolor sit amet, consectetur
                                                                                adipiscing elit. Cras vitae nibh nisl.
                                                                                Cras
                                                                                et ma uriseg haset lorem ultricies ferm
                                                                                entum ras vitae nibh nisl. Forem ipsum
                                                                                dolor
                                                                                sit amet. </p>
                                                                        </div>
                                                                        <!--/.comment-content -->
                                                                        <div class="reply">
                                                                            <a class="comment-reply-link"
                                                                               href="#">Reply</a>
                                                                        </div>
                                                                        <!--/.reply -->
                                                                    </div>
                                                                    <!-- /.comment-details -->
                                                                </div>
                                                                <!--/.comment-body -->
                                                                <ol class="children">
                                                                    <li class="comment">
                                                                        <div class="comment-body">
                                                                            <div class="comment-author vcard">
                                                                                <div class="author-img">
                                                                                    <img alt="author"
                                                                                         src="{{ url('public') }}/web_assets/assets/images/author/04.jpg">
                                                                                </div>
                                                                            </div>
                                                                            <!--/.comment-author -->
                                                                            <div class="comment-details">
                                                                                <div class="comment-metadata">
                                                                                    <b class="author-name">Nicole
                                                                                        Sperus</b>
                                                                                    <span class="date">April 14,2015 at 11:04</span>
                                                                                </div>
                                                                                <!--/.comment-metadata -->

                                                                                <div class="comment-content">
                                                                                    <p>Lorem ipsum dolor sit amet,
                                                                                        consectetur adipiscing elit.
                                                                                        Cras
                                                                                        vitae nibh nisl. Cras et ma
                                                                                        uriseg
                                                                                        haset lorem ultricies ferm entum
                                                                                        ras
                                                                                        vitae nibh nisl. Forem ipsum
                                                                                        dolor
                                                                                        sit amet.</p>
                                                                                </div>
                                                                                <!--/.comment-content -->
                                                                                <div class="reply">
                                                                                    <a class="comment-reply-link"
                                                                                       href="#">Reply</a>
                                                                                </div>
                                                                                <!--/.reply -->
                                                                            </div>
                                                                            <!-- /.comment-details -->
                                                                        </div>
                                                                        <!--/.comment-body -->
                                                                    </li>
                                                                </ol>
                                                            </li>
                                                        </ol>
                                                        <!--/.comment-list -->
                                                    </div>
                                                    <!--/.comment section-->


                                                </div>
                                                <!--/.comment's area-->


                                            </div>
                                            <!--/.comment's area-->
                                        </div>
                                    </div>
                                    <div id="tab3" class="tab-pane">
                                        <h3>Tab 3</h3>
                                        <p>At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd
                                            gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem
                                            ipsum
                                            dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero
                                            eos
                                            et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                                            sea
                                            takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet,
                                            consetetur sadipscing elitr, At accusam aliquyam diam diam dolore dolores
                                            duo
                                            eirmod eos erat, et nonumy sed tempor et et invidunt justo labore Stet clita
                                            ea
                                            et gubergren, kasd magna no rebum. sanctus sea sed takimata ut vero
                                            voluptua.
                                            est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="related-products">
                                <div class="heading-title">
                                    <h3 class="title-text">related products</h3>
                                </div>
                                <div class="related-product-content">
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="product-single">
                                            <div class="product-thumb">
                                                <img class="img-responsive" alt="Single product"
                                                     src="{{ url('public') }}/web_assets/assets/images/gallery_men/01.jpg">
                                                <div class="actions">
                                                    <ul>
                                                        <li><a class="add-cart" href="single-product.html"><span><span
                                                                            class="icon_plus"></span></span> add to cart</a>
                                                        </li>
                                                        <li><a href="#"><span class="icon_heart_alt"></span></a>
                                                        </li>
                                                        <li><a class="zoom"
                                                               href="{{ url('public') }}/web_assets/assets/images/gallery_men/01.jpg"><span
                                                                        class="arrow_expand"></span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!--/.product-thumb-->
                                            <div class="product-info">
                                                <h2>White Stripe T-Shirt</h2>
                                                <h4 class="product-name">T-Shirt</h4>
                                                <div class="price">
                                                    $40
                                                    <del> $50</del>
                                                </div>
                                            </div>
                                            <!--/.product-info-->
                                        </div>
                                        <!--/.product-single-->
                                    </div>
                                    <!--/.col-md-4-->
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="product-single">
                                            <div class="product-thumb">
                                                <img class="img-responsive" alt="Single product"
                                                     src="{{ url('public') }}/web_assets/assets/images/gallery_men/03.jpg">
                                                <div class="actions">
                                                    <ul>
                                                        <li><a class="add-cart" href="single-product.html"><span><span
                                                                            class="icon_plus"></span></span> add to cart</a>
                                                        </li>
                                                        <li><a href="#"><span class="icon_heart_alt"></span></a>
                                                        </li>
                                                        <li><a class="zoom"
                                                               href="{{ url('public') }}/web_assets/assets/images/gallery_men/03.jpg"><span
                                                                        class="arrow_expand"></span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!--/.product-thumb-->
                                            <div class="product-info">
                                                <h2>White Stripe T-Shirt</h2>
                                                <h4 class="product-name">T-Shirt</h4>
                                                <div class="price">
                                                    $40
                                                    <del> $50</del>
                                                </div>
                                            </div>
                                            <!--/.product-info-->
                                        </div>
                                        <!--/.product-single-->
                                    </div>
                                    <!--/.col-md-4-->
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="product-single">
                                            <div class="product-thumb">
                                                <img class="img-responsive" alt="Single product"
                                                     src="{{ url('public') }}/web_assets/assets/images/gallery_men/05.jpg">
                                                <div class="actions">
                                                    <ul>
                                                        <li><a class="add-cart" href="single-product.html"><span><span
                                                                            class="icon_plus"></span></span> add to cart</a>
                                                        </li>
                                                        <li><a href="#"><span class="icon_heart_alt"></span></a>
                                                        </li>
                                                        <li><a class="zoom"
                                                               href="{{ url('public') }}/web_assets/assets/images/gallery_men/05.jpg"><span
                                                                        class="arrow_expand"></span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!--/.product-thumb-->
                                            <div class="product-info">
                                                <h2>White Stripe T-Shirt</h2>
                                                <h4 class="product-name">T-Shirt</h4>
                                                <div class="price">
                                                    $40
                                                    <del> $50</del>
                                                </div>
                                            </div>
                                            <!--/.product-info-->
                                        </div>
                                        <!--/.product-single-->
                                    </div>
                                    <!--/.col-md-4-->
                                    <div class="col-md-3 col-sm-6 col-xs-6">
                                        <div class="product-single">
                                            <div class="product-thumb">
                                                <img class="img-responsive" alt="Single product"
                                                     src="{{ url('public') }}/web_assets/assets/images/gallery_men/08.jpg">
                                                <div class="actions">
                                                    <ul>
                                                        <li><a class="add-cart" href="single-product.html"><span><span
                                                                            class="icon_plus"></span></span> add to cart</a>
                                                        </li>
                                                        <li><a href="#"><span class="icon_heart_alt"></span></a>
                                                        </li>
                                                        <li><a class="zoom"
                                                               href="{{ url('public') }}/web_assets/assets/images/gallery_men/08.jpg"><span
                                                                        class="arrow_expand"></span></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <!--/.product-thumb-->
                                            <div class="product-info">
                                                <h2>White Stripe T-Shirt</h2>
                                                <h4 class="product-name">T-Shirt</h4>
                                                <div class="price">
                                                    $40
                                                    <del> $50</del>
                                                </div>
                                            </div>
                                            <!--/.product-info-->
                                        </div>
                                        <!--/.product-single-->
                                    </div>
                                    <!--/.col-md-4-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection