@extends('frontend.master')

@section('title')
    HOME || EBOOK
@endsection

@section('main_content')
    <div class="slider-area">
        <div class="container">
            <div class="tp-banner-container">
                <div class="tp-banner" id="tp-banner-1">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                            <!-- MAIN IMAGE --><img
                                    src="{{URL::to ('public/web_assets')}}/assets/images/slider/sliderHome1.jpg"
                                    alt="slidebg1"
                                    data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption smallHeading skewfromrightshort customout" data-x="700" data-y="96"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500" data-start="800" data-easing="Back.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 4">Awesome new looks
                                for women
                            </div>
                            <div class="tp-caption lightgrey_divider skewfromrightshort customout" data-x="800" data-y="140"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500" data-start="1200" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 2">
                            </div>
                            <!-- LAYER NR. 6 -->
                            <div class="tp-caption mainHeading skewfromleftshort customout" data-x="575" data-y="152"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="300" data-start="1100" data-easing="Back.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 7">WOMEN´S WINTER
                                FASHION
                            </div>
                            <!-- LAYER NR. 7 -->
                            <div class="tp-caption small_thin_para customin customout" data-x="575" data-y="240"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500" data-start="1300" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 8">Lorem ipsum dolor
                                sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                <br/> invidunt ut labore et dolore magna aliquyam
                                <br/>erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                            </div>
                            <div class="tp-caption gray-button customin customout" data-x="700" data-y="360"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700" data-start="1500" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 8"><a
                                        href="single-product.html">VIEW DETAILS</a>
                            </div>
                            <div class="tp-caption orange-button customin customout" data-x="860" data-y="360"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="900" data-start="1700" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 8"><a
                                        href="shop-grid.html">COLLECTION</a>
                            </div>
                        </li>
                        <!-- SLIDE  -->
                        <li data-transition="zoomout" data-slotamount="7" data-masterspeed="1000">
                            <!-- MAIN IMAGE --><img
                                    src="{{URL::to ('public/web_assets')}}/assets/images/slider/sliderHome2.jpg"
                                    alt="darkblurbg"
                                    data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption smallHeading skewfromrightshort customout" data-x="700" data-y="96"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500" data-start="800" data-easing="Back.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 4">Awesome new looks
                                for women
                            </div>
                            <div class="tp-caption lightgrey_divider skewfromrightshort customout" data-x="800" data-y="140"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500" data-start="1200" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 2">
                            </div>
                            <!-- LAYER NR. 6 -->
                            <div class="tp-caption mainHeading skewfromleftshort customout" data-x="575" data-y="152"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="300" data-start="1100" data-easing="Back.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 7">WOMEN´S WINTER
                                FASHION
                            </div>
                            <!-- LAYER NR. 7 -->
                            <div class="tp-caption small_thin_para customin customout" data-x="575" data-y="240"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="500" data-start="1300" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 8">Lorem ipsum dolor
                                sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
                                <br/> invidunt ut labore et dolore magna aliquyam
                                <br/>erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                            </div>
                            <div class="tp-caption gray-button customin customout" data-x="700" data-y="360"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="700" data-start="1500" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 8"><a
                                        href="single-product.html">VIEW DETAILS</a>
                            </div>
                            <div class="tp-caption orange-button customin customout" data-x="860" data-y="360"
                                 data-customin="x:0;y:100;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:1;scaleY:3;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:0% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="900" data-start="1700" data-easing="Power4.easeOut" data-endspeed="500"
                                 data-endeasing="Power4.easeIn" data-captionhidden="on" style="z-index: 8"><a
                                        href="shop-grid.html">COLLECTION</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <!--Latest ebook -->
        @include('frontend.dynamic_files.home_files.latest')
        <!--/.Latest ebook-->

        <!--/.Items-Carosel-Area-->
        <div class="items-carosel-area">
            <div class="row">
                @include('frontend.dynamic_files.home_files.top_rated')
                <!--/.pupoler-authors-->
                @include('frontend.dynamic_files.home_files.popular_authors')
                <!--/.letest-authors-->
                @include('frontend.dynamic_files.home_files.popular_publishers')
                <!--/.top-sellers-->
            </div>
            <!--/.row-->
        </div>
        <!--/.items-carosel-area-->
        <!--Latest Blogs-->
        @include('frontend.dynamic_files.home_files.latest_blogs')
        <!--/.latest blogs -->

    </div>
@endsection