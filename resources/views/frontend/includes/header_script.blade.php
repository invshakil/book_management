<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="{{URL::to ('public/web_assets')}}/assets/css/bootstrap.min.css">
    <!-- FONT AWESOME -->
    <link rel="stylesheet" href="{{URL::to ('public/web_assets')}}/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{URL::to ('public/web_assets')}}/assets/css/eleganticon.css">
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{URL::to ('public/web_assets')}}/assets/rs-plugin/css/style.css"
          media="screen"/>
    <link rel="stylesheet" type="text/css"
          href="{{URL::to ('public/web_assets')}}/assets/rs-plugin/css/navstylechange.css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{URL::to ('public/web_assets')}}/assets/rs-plugin/css/noneed.css"
          media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{URL::to ('public/web_assets')}}/assets/rs-plugin/css/settings.css"
          media="screen"/>
    <!-- MAGNIFIC POPUP -->
    <link rel="stylesheet" href="{{URL::to ('public/web_assets')}}/assets/css/magnific-popup.css">
    <!-- OWL CAROUSEL -->
    <link rel="stylesheet" href="{{URL::to ('public/web_assets')}}/assets/css/owl.carousel.css">
    <!-- ANIMATE CSS -->
    <link rel="stylesheet" href="{{URL::to ('public/web_assets')}}/assets/css/animate.css">
    <!-- STYLESHEET -->
    <link rel="stylesheet" href="{{URL::to ('public/web_assets')}}/style.css">
    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,500,600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:600,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Cookie:400' rel='stylesheet' type='text/css'>
    <!-- FAVICON -->
    <link rel="icon" href="{{URL::to ('public/web_assets')}}/assets/images/favicon.ico">
    <link rel="apple-touch-icon" href="{{URL::to ('public/web_assets')}}/assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72"
          href="{{URL::to ('public/web_assets')}}/assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="{{URL::to ('public/web_assets')}}/assets/images/apple-touch-icon-114x114.png">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <style>
        .star-rating {
            line-height:32px;
            font-size:1.25em;
        }

        .star-rating .fa-star{color: yellow;}
    </style>

    <![endif]-->
</head>