<div class="header-middle">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12 site-logo">
                <div class="logo">
                    <a href="{{url('/')}}"><img src="{{URL::to ('public/web_assets')}}/assets/images/logo.png"
                                               alt="logo"/>
                    </a>
                </div>
                <div class="cart-text"><i class="fa fa-2x fa-check-circle" aria-hidden="true"></i> READ FREE EBOOK
                </div>
            </div>
            <!--/.site-logo-->
            <div class="col-md-4 col-sm-4 col-xs-12 header-search">
                <div class="search default">
                    <form class="searchform" action="#" method="get">
                        <div class="input-group">
                            <input type="search" id="dsearch" name="s" class="form-control"
                                   placeholder="Search for something..."> <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" id="submit-btn"><span
                                                    class="arrow_right"></span>
                                    </button>
                                    </span>
                        </div>
                    </form>
                </div>
                <!--/.search-->
            </div>
            <!--/.header-search-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>