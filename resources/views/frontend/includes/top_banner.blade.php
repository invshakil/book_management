<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="left"> Welcome to EBOOK CENTER BD <span><i class="fa fa-phone"></i>Call us</span> +49 1234 5678 9
                </div>
                <!--/.left-->
            </div>
            <!--/.col-md-6-->
            <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="right">
                    <ul>
                        <li class="toggle">
                            <span>Account</span> <i class="fa fa-sign-in"></i>
                            <ul>
                                <li><a href="{{url('/register')}}">Register</a>
                                </li>
                                <li><a href="{{url('/login')}}">Login</a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <!--/.right-->
            </div>
            <!--/.col-md-6-->
        </div>
        <!--/.row-->
    </div>
    <!--/.container-->
</div>