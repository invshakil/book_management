<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class Author extends Controller
{
    //
    public function index()
    {
        $type = Session::get('login_type');

        if ($type != 2)
        {
            return Redirect::to('/login')->send();
        }

        return 'Author controller';
    }
}
