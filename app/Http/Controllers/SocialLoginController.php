<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\Services\SocialFacebookAccountService;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class SocialLoginController extends Controller
{
    /**
     * Create a redirect method to facebook api.
     *
     * return void
     */

    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Return a callback method from facebook api.
     *
     * return callback URL from facebook
     */

    public function callback(SocialFacebookAccountService $service)
    {
        $query = $service->createOrGetUser(Socialite::driver('facebook')->user());

        if ($query)
        {

            if ($query->user_type == 1)
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',1);
                return Redirect::to('admin_dashboard');
            }
            elseif($query->user_type == 2)
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',2);
                return Redirect::to('author_dashboard');
            }
            elseif($query->user_type == 3)
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',3);
                return Redirect::to('publisher_dashboard');
            }
            else
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',4);
                return Redirect::to('user_dashboard');
            }
        }
    }
}
