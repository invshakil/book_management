<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class UserLogin extends Controller
{
    public function index()
    {

        $type = Session::get('login_type');

        if ($type == 1){
            return Redirect::to('/admin_dashboard');
        }
        if ($type == 2){
            return Redirect::to('/author_dashboard');
        }
        if ($type == 3){
            return Redirect::to('/publisher_dashboard');
        }
        if ($type == 4){
            return Redirect::to('/user_dashboard');
        }

        $data = array();

        $data['page_title'] = 'User Login || Ebook';

        return view('backend.login',$data);
    }

    public function ajax_login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $email = $request->email;
        $password = md5($request->password);

        $query = DB::table('users')
            ->where('email', $email)
            ->where('password', $password)
            ->first();

        if ($query)
        {

            if ($query->user_type == 1)
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',1);
                return Redirect::to('admin_dashboard');
            }
            elseif($query->user_type == 2)
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',2);
                return Redirect::to('author_dashboard');
            }
            elseif($query->user_type == 3)
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',3);
                return Redirect::to('publisher_dashboard');
            }
            else
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',4);
                return Redirect::to('user_dashboard');
            }
        }
        else
        {
            Session::put('exception', 'Invalid User Email or Password! Try Again.');
            return Redirect::to('login');
        }

    }


    public function logout()
    {
        Session::put('id',null);
        Session::put('name',null);
        Session::put('login_type', null);

        Session::put('message', 'You Are Successfully Logged Out!');

        return Redirect::to('/login');

    }
}
