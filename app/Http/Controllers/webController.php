<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;

class webController extends Controller
{
    public function index()
    {
        $data = array();
        $data['title'] = 'Home';
        $data['latest_books'] = DB::table('books')
            ->latest()
            ->limit(9)
            ->join('categories', 'books.category', '=', 'categories.category_id')
            ->join('authors', 'books.author', '=', 'authors.authors_id')
            ->select('books.*','categories.category_name', 'authors.authors_name')
            ->get();
        $page = view('frontend.dynamic_files.home',$data);
        return view('frontend.master',$data)->with('page',$page);
    }
    /*
     * BOOK DETAILS FUNCTION
     */

    public function bookDetails($id)
    {
        $data = array();
        $book_id = base64_decode($id);
        $data['book_details'] = DB::table('books')
            ->where('book_id',$book_id)
            ->join('categories', 'books.category', '=', 'categories.category_id')
            ->join('publishers', 'books.publisher', '=', 'publishers.publishers_id')
            ->join('authors', 'books.author', '=', 'authors.authors_id')
            ->select('books.*','categories.category_name', 'publishers.publishers_name', 'authors.authors_name')
            ->first();
        $data['title'] = $data['book_details']->name.' || EBOOK';

        $page = view('frontend.dynamic_files.book_details',$data);
        return view('frontend.master',$data)->with('page',$page);
    }
}
