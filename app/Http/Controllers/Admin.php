<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class Admin extends Controller
{
    public function index()
    {
        $type = Session::get('login_type');

        if ($type != 1) {
            return Redirect::to('/login')->send();
        }

        $data = array();
        $data['page_name'] = 'Admin Dashboard';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.dashboard', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    /*
     * PROFILE FUNCTION
     */

    public function profile(Request $request, $param = NULL, $param2 = NULL)
    {
        $type = Session::get('login_type');

        if ($type != 1) {
            return Redirect::to('/login')->send();
        }

        $data = array();
        $data['page_name'] = 'Admin Profile';

        //Image Upload//

        if ($param == 'image') {
            $id = $request->input('id');
            $image = DB::table('users')->where('id', $id)->value('image_path');

            //FIRST WILL CHECK IF USER HAS ANY UPLOADED IMAGE
            if ($image == NULL) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:512',
                ]);

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/profile_images/'), $imageName);

                $insert = array('id' => $id,
                    'image_path' => 'public/uploads/profile_images/' . $imageName,
                );

                DB::table('users')->insert($insert);

                Session::put('message', 'Image Uploaded Successfully!');

                return back();

            } else {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:512',
                ]);

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/profile_images/'), $imageName);

                $update = array('image_path' => 'public/uploads/profile_images/' . $imageName);

                DB::table('users')
                    ->where('id', $id)
                    ->update($update);

                Session::put('message', 'Image Updated Successfully!');

                return back();
            }
        } elseif ($param == 'do_update') {
            $id = $request->input('id');
            $info = DB::table('users')->where('id', $id)->value('id');


            if ($info == NULL) {
                $this->validate($request, [
                    'profession' => 'required',
                    'address' => 'required',
                    'phone' => 'required'
                ]);

                $insert = array('id' => $id,
                    'profession' => $request->input('profession'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone')
                );

                DB::table('users')->insert($insert);

                Session::put('message', 'Profile Information Saved Successfully!');

                return back();

            } else {

                $this->validate($request, [
                    'profession' => 'required',
                    'address' => 'required',
                    'phone' => 'required'
                ]);

                $data = array(
                    'profession' => $request->input('profession'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone')
                );

                DB::table('users')->where('id', $id)->update($data);

                Session::put('message', 'Profile Information Updated Successfully!');

                return back();
            }
        } elseif ($param == 'password') {
            $this->validate($request, [
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password'
            ]);

            $data = array(
                'password' => $request->input('password')
            );
            $id = $request->input('id');
            DB::table('users')->where('id', $id)->update($data);


            return back();
        }


        $data['account_type'] = 'admin';
        $page = view('backend.admin.profile', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    /*
     * BOOK CATEGORY FUNCTION
     */

    public function bookCategory(Request $request, $param = NULL, $param2 = NULL)
    {
        $type = Session::get('login_type');

        if ($type != 1) {
            return Redirect::to('/login')->send();
        }

        if ($param == 'create') {
            $this->validate($request, [
                'category_name' => 'required',
                'category_description' => 'nullable',
                'status' => 'required'
            ]);

            $data = array(
                'category_name' => $request->input('category_name'),
                'category_description' => $request->input('category_description'),
                'status' => $request->input('status')
            );
            Session::put('message', 'Category Added Successfully!');

            DB::table('categories')->insert($data);

            return back();
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'category_name' => 'required',
                'category_description' => 'required',
                'status' => 'required'
            ]);

            $data = array(
                'category_name' => $request->input('category_name'),
                'category_description' => $request->input('category_description'),
                'status' => $request->input('status')
            );
            Session::put('message', 'Category Updated Successfully!');

            DB::table('categories')->where('category_id', $param2)->update($data);

            return back();
        } elseif ($param == 'delete') {
            DB::table('categories')->where('category_id', $param2)->delete();
            Session::put('exception', 'Category Deleted Successfully!');
            return back();
        }

        $data = array();
        $data['page_name'] = 'Book Categories';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.book_category', $data);

        return view('backend.master', $data)->with('page', $page);

    }

    /*
     * PUBLISHERS FUNCTION
     */

    public function publishers(Request $request, $param = NULL, $param2 = NULL)
    {
        $type = Session::get('login_type');

        if ($type != 1) {
            return Redirect::to('/login')->send();
        }

        if ($param == 'create') {
            $this->validate($request, [
                'publishers_name' => 'required',
                'address' => 'nullable',
                'phone' => 'nullable',
                'email' => 'nullable',
                'website' => 'nullable',
                'image' => 'nullable|mimes:jpeg,bmp,png'
            ]);

            if ($request->image != NULL) {

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/publishers_logo/'), $imageName);

                $data = array(
                    'publishers_name' => $request->input('publishers_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'website' => $request->input('website'),
                    'status' => $request->input('status'),
                    'logo' => 'public/uploads/publishers_logo/' . $imageName
                );

                DB::table('publishers')->insert($data);
            } else {

                $data = array(
                    'publishers_name' => $request->input('publishers_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'website' => $request->input('website'),
                    'status' => $request->input('status')
                );

                DB::table('publishers')->insert($data);
            }

            Session::put('message', 'Publishers Information Added Successfully!');

            return back();
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'publishers_name' => 'required',
                'address' => 'nullable',
                'phone' => 'nullable',
                'email' => 'nullable',
                'website' => 'nullable',
                'image' => 'nullable|mimes:jpeg,bmp,png'
            ]);

            if ($request->image != NULL) {

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/publishers_logo/'), $imageName);

                $data = array(
                    'publishers_name' => $request->input('publishers_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'website' => $request->input('website'),
                    'status' => $request->input('status'),
                    'logo' => 'public/uploads/publishers_logo/' . $imageName
                );

                DB::table('publishers')->where('publishers_id', $param2)->update($data);
            } else {

                $data = array(
                    'publishers_name' => $request->input('publishers_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'website' => $request->input('website'),
                    'status' => $request->input('status')
                );


                DB::table('publishers')->where('publishers_id', $param2)->update($data);
            }

            Session::put('message', 'Publishers Information Updated Successfully!');

            return back();
        } elseif ($param == 'delete') {
            DB::table('publishers')->where('publishers_id', $param2)->delete();
            Session::put('exception', 'Publishers Information Deleted Successfully!');
            return back();
        }

        $data = array();
        $data['page_name'] = 'Book Publishers';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.publishers', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    /*
     * AUTHORS FUNCTION
     */

    public function authors(Request $request, $param = NULL, $param2 = NULL)
    {
        $type = Session::get('login_type');

        if ($type != 1) {
            return Redirect::to('/login')->send();
        }

        if ($param == 'create') {
            $this->validate($request, [
                'authors_name' => 'required',
                'address' => 'nullable',
                'phone' => 'nullable',
                'email' => 'nullable',
                'biography' => 'nullable',
                'image' => 'nullable|mimes:jpg,jpeg,bmp,png'
            ]);

            if ($request->image != NULL) {

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/authors_image/'), $imageName);

                $data = array(
                    'authors_name' => $request->input('authors_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'biography' => $request->input('biography'),
                    'status' => $request->input('status'),
                    'image' => 'public/uploads/authors_image/' . $imageName,
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                DB::table('authors')->insert($data);
            } else {

                $data = array(
                    'authors_name' => $request->input('authors_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'biography' => $request->input('biography'),
                    'status' => $request->input('status'),
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                DB::table('authors')->insert($data);
            }

            Session::put('message', 'Authors Information Added Successfully!');

            return back();
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'authors_name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'email' => 'required',
                'biography' => 'required',
                'image' => 'nullable|mimes:jpg,jpeg,bmp,png'
            ]);

            if ($request->image != NULL) {

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/authors_image/'), $imageName);

                $data = array(
                    'authors_name' => $request->input('authors_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'biography' => $request->input('biography'),
                    'status' => $request->input('status'),
                    'image' => 'public/uploads/authors_image/' . $imageName,
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                DB::table('authors')->where('authors_id', $param2)->update($data);
            } else {

                $data = array(
                    'authors_name' => $request->input('authors_name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'email' => $request->input('email'),
                    'biography' => $request->input('biography'),
                    'status' => $request->input('status'),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );


                DB::table('authors')->where('authors_id', $param2)->update($data);
            }

            Session::put('message', 'Authors Information Updated Successfully!');

            return back();
        } elseif ($param == 'delete') {
            DB::table('authors')->where('authors_id', $param2)->delete();
            Session::put('exception', 'Authors Information Deleted Successfully!');
            return back();
        }

        $data = array();
        $data['page_name'] = 'Book Authors';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.authors', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    /**
     ** BOOKS UPLOAD AND MANAGE
     **/

    public function books(Request $request, $param = NULL, $param2 = NULL)
    {
        $type = Session::get('login_type');

        if ($type != 1) {
            return Redirect::to('/login')->send();
        }

        if ($param == 'create') {
            $this->validate($request, [
                'name' => 'required',
                'image' => 'required',
                'publisher' => 'required',
                'author' => 'required',
                'category' => 'required',
                'meta_title' => 'nullable',
                'meta_description' => 'nullable',
                'file' => 'required|mimes:pdf'
            ]);

            $photo = $request->file('image');
            $image_name = time() . '.' . $photo->getClientOriginalExtension();

            $destinationPath1 = public_path('/uploads/book_cover/re_sized/');
            $thumb_img = Image::make($photo->getRealPath())->resize(100, 150);
            $thumb_img->save($destinationPath1 . '/' . $image_name, 80);

            $destinationPath2 = public_path('/uploads/book_cover');
            $photo->move($destinationPath2, $image_name);

            $file = $request->file('file');
            $file_name = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path('/uploads/files/');
            $file->move($destinationPath, $file_name);

            $data = array(
                'name' => $request->input('name'),
                'publisher' => $request->input('publisher'),
                'author' => $request->input('author'),
                'category' => $request->input('category'),
                'description' => $request->input('description'),
                'meta_title' => $request->input('meta_title'),
                'meta_description' => $request->input('meta_description'),
                'status' => $request->input('status'),
                'image' => 'public/uploads/book_cover/' . $image_name,
                'thumbnail' => 'public/uploads/book_cover/re_sized/' . $image_name,
                'file' => 'public/uploads/files/' . $file_name,
                'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            DB::table('books')->insert($data);

            Session::put('message', 'Books Information Added Successfully!');

            return back();
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'name' => 'required',
                'publisher' => 'required',
                'author' => 'required',
                'category' => 'required',
                'meta_title' => 'nullable',
                'meta_description' => 'nullable'
            ]);

            // IF USER UPLOAD NEW BOOK AND ADD NEW COVER
            if ($request->image != NULL && $request->file != NULL) {
                $photo = $request->file('image');
                $image_name = time() . '.' . $photo->getClientOriginalExtension();

                $destinationPath1 = public_path('/uploads/book_cover/re_sized/');
                $thumb_img = Image::make($photo->getRealPath())->resize(100, 150);
                $thumb_img->save($destinationPath1 . '/' . $image_name, 80);

                $destinationPath2 = public_path('/uploads/book_cover');
                $photo->move($destinationPath2, $image_name);

                $file = $request->file('file');
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/files/');
                $file->move($destinationPath, $file_name);

                $data = array(
                    'name' => $request->input('name'),
                    'publisher' => $request->input('publisher'),
                    'author' => $request->input('author'),
                    'category' => $request->input('category'),
                    'description' => $request->input('description'),
                    'meta_title' => $request->input('meta_title'),
                    'meta_description' => $request->input('meta_description'),
                    'status' => $request->input('status'),
                    'image' => 'public/uploads/book_cover/' . $image_name,
                    'thumbnail' => 'public/uploads/book_cover/re_sized/' . $image_name,
                    'file' => 'public/uploads/files/' . $file_name,
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );
            }
            // IF USER UPLOAD NEW BOOK AND BUT DOES NOT UPLOAD NEW BOOK
            elseif ($request->image != NULL && $request->file == NULL)
            {
                $photo = $request->file('image');
                $image_name = time() . '.' . $photo->getClientOriginalExtension();

                $destinationPath1 = public_path('/uploads/book_cover/re_sized/');
                $thumb_img = Image::make($photo->getRealPath())->resize(100, 150);
                $thumb_img->save($destinationPath1 . '/' . $image_name, 80);

                $destinationPath2 = public_path('/uploads/book_cover');
                $photo->move($destinationPath2, $image_name);

                $data = array(
                    'name' => $request->input('name'),
                    'publisher' => $request->input('publisher'),
                    'author' => $request->input('author'),
                    'category' => $request->input('category'),
                    'description' => $request->input('description'),
                    'meta_title' => $request->input('meta_title'),
                    'meta_description' => $request->input('meta_description'),
                    'status' => $request->input('status'),
                    'image' => 'public/uploads/book_cover/' . $image_name,
                    'thumbnail' => 'public/uploads/book_cover/re_sized/' . $image_name,
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );
            }

            // IF USER DOES NOT UPLOAD NEW BOOK AND BUT UPLOADS NEW BOOK

            elseif ($request->image == NULL && $request->file != NULL){
                $file = $request->file('file');
                $file_name = time() . '.' . $file->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/files/');
                $file->move($destinationPath, $file_name);

                $data = array(
                    'name' => $request->input('name'),
                    'publisher' => $request->input('publisher'),
                    'author' => $request->input('author'),
                    'category' => $request->input('category'),
                    'description' => $request->input('description'),
                    'meta_title' => $request->input('meta_title'),
                    'meta_description' => $request->input('meta_description'),
                    'status' => $request->input('status'),
                    'file' => 'public/uploads/files/' . $file_name,
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );
            }
            //IF THERE IS NO NEW UPLOAD OF IMAGE OR FILE
            else{

                $data = array(
                    'name' => $request->input('name'),
                    'publisher' => $request->input('publisher'),
                    'author' => $request->input('author'),
                    'category' => $request->input('category'),
                    'description' => $request->input('description'),
                    'meta_title' => $request->input('meta_title'),
                    'meta_description' => $request->input('meta_description'),
                    'status' => $request->input('status'),
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );
            }


            DB::table('books')->where('book_id', $param2)->update($data);

            Session::put('message', 'Books Information Updated Successfully!');

            return back();
        } elseif ($param == 'delete') {
            DB::table('authors')->where('authors_id', $param2)->delete();
            Session::put('exception', 'Books Information Deleted Successfully!');
            return back();
        }

        $data = array();
        $data['page_name'] = 'Book Authors';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.books', $data);

        return view('backend.master', $data)->with('page', $page);
    }
}
